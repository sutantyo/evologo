turtles-own [life age output move reproduce mortality sensitivity]
patches-own [PepMax]

Breed[Red-As Red-A]
Breed[Blue-As Blue-A]
Breed[White-As White-A]
;;Creates breeds of each colour
Breed[Red-Peps Red-Pep]
Breed[Blue-Peps Blue-Pep]
Breed[White-Peps White-Pep]
;;Creates breeds for each peptide colour

;;BUTTON FUNCTIONS
to setup
  clear-all
  random-seed rseed
  create-red
  create-blue
  create-white
  reset-ticks
end

to create-red
  create-Red-As Red-Agents ;;Creates breed specific turtles
  [ ask Red-As
    [setxy random-xcor random-ycor
    set color red
    set shape "circle"
    set life Lifespan
    set age 0
    set move Move-Distance
    set reproduce Reproduction-Chance
    set mortality Mortality-Rate]
    set sensitivity (4 * Red-Peptide-Creation)]

end

to create-blue
  create-Blue-As Blue-Agents
   [
    setxy random-xcor random-ycor
    set color blue
    set shape "circle"
    set life Lifespan
    set age 0
    set move Move-Distance
    set reproduce Reproduction-Chance
    set mortality Mortality-Rate
    set sensitivity (4 * Blue-Peptide-Creation)]
end

to create-white
  create-White-As White-Agents
   [
    setxy random-xcor random-ycor
    set color white
    set shape "circle"
    set life Lifespan
    set age 0
    set move Move-Distance
    set reproduce Reproduction-Chance
    set mortality Mortality-Rate
    set sensitivity (4 * White-Peptide-Creation)]
end

to check-peptides ;;patch recognition of the most common peptide present on patch currently with linked pcolor
  ask patches [
    if Area-peptides = 0 [set Pepmax 0]
    if (Area-Peptides > 0) and (Area-Peptides = count Red-Peps in-radius 1) [set Pepmax 1]
    if (Area-Peptides > 0) and (Area-Peptides = count blue-Peps in-radius 1) [set Pepmax 2]
    if (Area-Peptides > 0) and (Area-Peptides = count white-Peps in-radius 1) [set PepMax 3];;pepmax key: 0= mostly red peptides 1= mostly blue 2= mostly white
]
end
